var banner = document.querySelector('#banner');
var design = document.querySelector('#design');
var development = document.querySelector('#development');
var maintain = document.querySelector('#maintain');
var middle = document.querySelector('#middle');
var designBtn = document.querySelector('#design-btn');
var developmentBtn = document.querySelector('#development-btn');
var maintainBtn = document.querySelector('#maintain-btn');
document.getElementById("year").innerHTML = new Date().getFullYear();

var btnIndex = new Array(3).fill(false);
var btnValue = [designBtn, developmentBtn, maintainBtn]

let changeBtn = idx => {
    btnIndex[0] = btnIndex[1] = btnIndex[2] = false;
    if (idx != -1) btnIndex[idx] = true;
    console.log(btnIndex)

    for(var i = 0; i < btnIndex.length; i++)
    {
        if(btnIndex[i]){
            btnValue[i].classList.add("bg-black", "text-white");
        }else{
            btnValue[i].classList.remove("bg-black", "text-white");
        }
    }
};

var a = new IntersectionObserver(function(entry) {
	if(entry[0].isIntersecting)
    changeBtn(0);
}, { threshold: [0.5] });

var b = new IntersectionObserver(function(entry) {
	if(entry[0].isIntersecting)
	changeBtn(1);
}, { threshold: [1] });

var c = new IntersectionObserver(function(entry) {
	if(entry[0].isIntersecting)
    changeBtn(2);
}, { threshold: [0.8] });

var d = new IntersectionObserver(function(entry) {
	if(entry[0].isIntersecting)
    changeBtn(-1);
}, { threshold: [1] });

var e = new IntersectionObserver(function(entry) {
	if(entry[0].isIntersecting)
    changeBtn(-1);
}, { threshold: [1] });

a.observe(design);
b.observe(development);
c.observe(maintain);
d.observe(middle);
d.observe(banner);