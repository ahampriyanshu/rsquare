# Resources


## Swipe.JS

1. [Swipe/flip Scrolling + tilt](sjs/2)
1. [Article Stack](sjs/1)

## GreenSock(GSAP)

1. [Verical Scrolling](gsap/1)
1. [Parallax](gsap/2)
1. [Animated Borders](gsap/3)
1. [Horizonatal List](gsap/4)
1. [SVG Animation](gsap/5)
1. [Pagination](gsap/6)
1. [Ad/Banner/Promotion](gsap/7)
1. [Download Button](gsap/8)
1. [Animated Heading](gsap/9)

## Buttons

1. [Magnetic ](resources/16)
1. [Retro ](resources/17)
1. [Set 1](resources/14)
1. [Set 2](resources/31)
1. [Set 3](resources/34)
1. [Particle effects](resources/38)
1. [Loading/Processing](resources/37)
1. [Boder Fade](resources/32)
1. [Boder Fill](resources/36)
1. [Sliding Effect](resources/33)
1. [Flip](resources/35)

## Cursor

1. [Cursor.js 1](resources/25)
1. [Cursor.js 2](resources/26)
1. [Cursor.js 3](resources/27)
1. [Flicker.js](resources/28)
1. [Stucked](resources/29)
1. [Emoji](resources/30)

## Image/Div/Tile/Container

1. [Titl + Glare + Follow](resources/1)
1. [Morphing Background](resources/39)
1. [Follow Only](resources/2)
1. [Segment + Follow](resources/3)
1. [Tilt.js Demo](resources/4)
1. [fx.js Demo](resources/5)
1. [Hover Set 1](resources/7)
1. [Hover Set 2](resources/7/2.index)
1. [Caption Effects](resources/8)
1. [Anime.Js](resources/10)

## Text/Headings/Typography

1. [Set 1](resources/6)

## Menu/List/Links

1. [Navbar Effects](resources/11)
1. [Menu Effects 1](resources/19/dist)
1. [Menu Effects 2](resources/20/dist)
1. [Menu Effects 3](resources/20/dist/2.html)
1. [Blurr Transition](resources/24/dist)

## Tooltip

1. [Set 1](resources/18)

## Scrolling/Zooming/Stroking/Underlining

1. [Horizontal Effect](resources/21/dist)
1. [Unerline + Stroke](resources/22)
1. [Trailing Marquee](resources/23/dist)